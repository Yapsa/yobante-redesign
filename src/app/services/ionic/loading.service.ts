import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  constructor(public loadingCtrl: LoadingController) { }

/*   presentLoading() {
   const loading = this.loadingCtrl.create({
      content: this.langageService.toTranslate("BUTTON.LOADING"),
      duration: 3000
    });

    loading.present();
  }
 */

  async presentLoadingWithOptions() {
    const loading = await this.loadingCtrl.create({
     // spinner: null,
      duration: 3000,
      message: 'Click the backdrop to dismiss early...',
      translucent: true,
      cssClass: 'custom-class custom-loading',
      backdropDismiss: true
    });
    loading.dismiss('dat1', 'role1');
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed with role:', role);
  }

}
