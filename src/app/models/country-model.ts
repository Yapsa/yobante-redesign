export interface Countries {
  code: string;
  code3: string;
  name: string;
  flag: string;
}
