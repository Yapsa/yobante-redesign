import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  toShowLoop = './assets/icon/black-loop.svg';
  toShowflag = './assets/icon/black-flag.svg';


  constructor( private router: Router) { }

  ngOnInit() {
  }

  logout(){
    console.log('---logout----');
    this.router.navigate(['login']);
  }
  selectLoopIcon() {
      this.toShowLoop = './assets/icon/white-loop.svg';
  }
  selectTripIcon() {
    if (this.toShowflag === './assets/icon/black-flag.svg') {
      this.toShowflag= './assets/icon/white-flag.svg';
    } else {
      this.toShowflag = './assets/icon/black-flag.svg';
    }
  }
}
