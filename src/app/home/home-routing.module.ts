import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    children:[
      {
        path: 'notifications',
        loadChildren: () =>
        import('../pages/notifications/notifications.module').then(m => m.NotificationsPageModule),
      },
      {
        path: 'colis',
        loadChildren: () =>
        import('../pages/colis/colis.module').then(m => m.ColisPageModule)
      },
      {
        path: 'credit',
        loadChildren: () =>
        import('../pages/credit/credit.module').then(m => m.CreditPageModule)
      },
      {
        path: 'trajets',
        loadChildren: () =>
        import('../pages/trajets/trajets.module').then(m => m.TrajetsPageModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {}
