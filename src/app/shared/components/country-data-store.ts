import { Countries } from 'src/app/models/country-model';

export const countries: Countries [] = [
    { code: 'SN', code3: 'SEN', name: 'Sénégal' , flag: './assets/images/senegal.png'},
    { code: 'NG', code3: 'NGA', name: 'Nigeria', flag: './assets/images/senegal.png' },
    { code: 'ZA', code3: 'ZAF', name: 'South Africa', flag: './assets/images/senegal.png'}
];
