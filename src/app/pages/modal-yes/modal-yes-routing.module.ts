import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalYesPage } from './modal-yes.page';

const routes: Routes = [
  {
    path: '',
    component: ModalYesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalYesPageRoutingModule {}
