import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalYesPageRoutingModule } from './modal-yes-routing.module';

import { ModalYesPage } from './modal-yes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalYesPageRoutingModule
  ],
  declarations: [ModalYesPage]
})
export class ModalYesPageModule {}
