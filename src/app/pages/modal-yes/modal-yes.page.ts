import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { YesModal } from 'src/app/models/yes-modal';

@Component({
  selector: 'app-modal-yes',
  templateUrl: './modal-yes.page.html',
  styleUrls: ['./modal-yes.page.scss'],
})
export class ModalYesPage implements OnInit {


  modalData: YesModal;
  constructor( private navParams: NavParams,
    public modalCtrl: ModalController,) {
    console.log('--MODAL navParams---', navParams.get('yesModal'));
      this.modalData = navParams.get('yesModal');
      console.log('--MODAL PAGE---', this.modalData);
  }

  ngOnInit() {
  }
  fermer(){
    console.log('--fermer-');
    this.dismiss();
  }


  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      dismissed: true,
      data: this.modalData
    });
  }
}
