import { Component, OnInit } from '@angular/core';
import { ActionSheetController, ModalController } from '@ionic/angular';
import { YesModal } from 'src/app/models/yes-modal';
import { ModalYesPage } from '../modal-yes/modal-yes.page';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

  data: Array<{title: string; details: string; textButton: string; showDetails: boolean}> = [];


  constructor(public actionSheetController: ActionSheetController,
    public modalCtrl: ModalController) {
      for(let i = 0; i < 2; i++ ){
        this.data.push({
            title: 'YES8888801'+ i,
            details: ' idatat non proident, sunt in culpa qui of',
            textButton: 'READ',
            showDetails: false
          });
      }
    }
  ngOnInit() {
  }
  parcelNumber(){
    const modalData: YesModal= {
      title: 'Fill in the parcel number',
      type: ''
    };
    this.presentModal(modalData);
  }

  scanParcel(){
    const modalData: YesModal = {
      title: 'Scan the parcel',
      type: ''
    };
    this.presentModal(modalData);
  }


  async scanColis(){
    console.log('--retrait--');
    const actionSheet = await this.actionSheetController.create({
      header: 'Pick up a parcel',
      cssClass: 'action-sheet-title',
      buttons: [{
        cssClass: 'myActionSheetBtnStyle',
        text: 'Scan the parcel',
        icon: 'scan-outline',
        handler: () => {
          this.scanParcel();
        }
      }, {
        cssClass: 'myActionSheetBtnStyle',
        text: 'Fill in the parcel number',
        icon: './assets/icon/box.svg',
        handler: () => {
          this.parcelNumber();
        }
      },
      {
        cssClass: 'close',
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  async presentModal(yesModal: YesModal) {
    const modal = await this.modalCtrl.create({
      component: ModalYesPage,
      componentProps: {
        // eslint-disable-next-line object-shorthand
        yesModal: yesModal
      }
    });
    return await modal.present();
  }

  readMessage(data) {
    if (data.showDetails) {
        data.showDetails = false;
        data.textButton = 'READ';
    } else {
        data.showDetails = true;
        data.textButton = 'MARK AS READ';
    }
  }

}
