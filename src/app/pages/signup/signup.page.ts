import { Component, OnInit, ViewChildren } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

 countries: any = [
    {
      code: '+221',
      name: 'SN'
    },
    {
      code: '+234',
      name: 'NG'
    }
  ];

  // eslint-disable-next-line @typescript-eslint/member-ordering
  constructor() { }

  ngOnInit() {
  }

}
