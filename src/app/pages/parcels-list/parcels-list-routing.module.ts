import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParcelsListPage } from './parcels-list.page';

const routes: Routes = [
  {
    path: '',
    component: ParcelsListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParcelsListPageRoutingModule {}
