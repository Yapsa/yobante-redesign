import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParcelsListPageRoutingModule } from './parcels-list-routing.module';

import { ParcelsListPage } from './parcels-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParcelsListPageRoutingModule
  ],
  declarations: [ParcelsListPage]
})
export class ParcelsListPageModule {}
