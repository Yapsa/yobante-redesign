import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-parcels-list',
  templateUrl: './parcels-list.page.html',
  styleUrls: ['./parcels-list.page.scss'],
})
export class ParcelsListPage implements OnInit {

  data: Array<{title: string; details: string; textButton: string; showDetails: boolean; weight: string; descr: string; type: string}> = [];

  constructor(public navCtrl: NavController) {
      this.data.push({
          title: 'YESDKR001',
          details: ' idatat non proident',
          textButton: 'SEE DETAILS',
          weight: '400 g',
          type: 'BUSINESS',
          descr: 'Macbook',
          showDetails: false
        });
    }

  ngOnInit() {
  }
  toggleDetails(data) {
    if (data.showDetails) {
        data.showDetails = false;
        data.textButton = 'SEE DETAILS';
    } else {
        data.showDetails = true;
        data.textButton = 'HIDE DETAILS';
    }
  }
}
