import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParcelsDetailsPage } from './parcels-details.page';

const routes: Routes = [
  {
    path: '',
    component: ParcelsDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParcelsDetailsPageRoutingModule {}
