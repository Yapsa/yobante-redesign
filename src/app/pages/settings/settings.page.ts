/* eslint-disable @typescript-eslint/no-unused-expressions */
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  private barcodeExpanded = false;
  private transportExpanded = false;
  private userExpanded = false;
  private opened = false;
  private currentNumber = 20;
  private items = [
    {name: 'item 1'},
    {name: 'item 2'},
    {name: 'item 4'},
    {name: 'item 5'}
  ];

  constructor() { }

  ngOnInit() {
  }
  showBarcode() {
    this.barcodeExpanded = this.barcodeExpanded === false;
  }
  transportDetails() {
    this.transportExpanded = this.transportExpanded === false;
  }
  userDetails() {
    this.userExpanded = this.userExpanded === false;
  }
  changePassword(){
    this.opened = !this.opened ;
  }
  increment() {
    this.currentNumber++;
  }

  private decrement() {
    this.currentNumber--;
  }
}
