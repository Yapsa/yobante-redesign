import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResetRedirectionPageRoutingModule } from './reset-redirection-routing.module';

import { ResetRedirectionPage } from './reset-redirection.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResetRedirectionPageRoutingModule
  ],
  declarations: [ResetRedirectionPage]
})
export class ResetRedirectionPageModule {}
