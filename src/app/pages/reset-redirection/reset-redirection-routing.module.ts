import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResetRedirectionPage } from './reset-redirection.page';

const routes: Routes = [
  {
    path: '',
    component: ResetRedirectionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResetRedirectionPageRoutingModule {}
