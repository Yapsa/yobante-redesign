import { Component, OnInit } from '@angular/core';
import { YesModal } from 'src/app/models/yes-modal';
import { ModalController, ActionSheetController, NavController } from '@ionic/angular';
import { ModalYesPage } from '../modal-yes/modal-yes.page';

@Component({
  selector: 'app-credit',
  templateUrl: './credit.page.html',
  styleUrls: ['./credit.page.scss'],
})
export class CreditPage implements OnInit {

  // eslint-disable-next-line @typescript-eslint/naming-convention
  private expansion= false;
   // eslint-disable-next-line @typescript-eslint/member-ordering
   balanceClicked = true;
   // eslint-disable-next-line @typescript-eslint/member-ordering
   showAmount = true;
  private items = [
    {name: 'item 1'},
    {name: 'item 2'},
    {name: 'item 4'},
    {name: 'item 5'}
  ];

    constructor(public actionSheetController: ActionSheetController, public navCtrl: NavController,
    public modalCtrl: ModalController) { }

  ngOnInit() {
  }
  retraitPointRelais(){
    const modalData: YesModal = {
      title: 'Withdrawal at the relay point',
      type: 'RetraitCash'
    };
    this.presentModal(modalData);
  }



  retraitPaydounya(){
    const modalData: YesModal = {
      title: 'Online withdrawal',
      type: 'RetraitPaydounya'
    };
    this.presentModal(modalData);
  }
// eslint-disable-next-line @typescript-eslint/naming-convention




  async retrait(){
    console.log('--retrait--');
    const actionSheet = await this.actionSheetController.create({
      header: 'Money Withdrawal',
      cssClass: 'action-sheet-title',
      buttons: [{
        cssClass: 'myActionSheetBtnStyle',
        text: 'Withdrawal at the relay point',
        icon: 'cash-outline',
        handler: () => {
          this.retraitPointRelais();
        }
      }, {
        cssClass: 'myActionSheetBtnStyle',
        text: 'Online withdrawal',
        icon: 'card-outline',
        handler: () => {
          this.retraitPaydounya();
        }
      },
      {
        cssClass: 'close',
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  async presentModal(yesModal: YesModal) {
    const modal = await this.modalCtrl.create({
      component: ModalYesPage,
      componentProps: {
        // eslint-disable-next-line object-shorthand
        yesModal: yesModal
      }
    });
    return await modal.present();
  }

  toggleAccordion() {
    this.expansion = this.expansion === false;
  }
  showBalance() {

    this.balanceClicked = !this.balanceClicked;
  }
  showTransaction(){
    this.showAmount = !this.showAmount;
  }

}
