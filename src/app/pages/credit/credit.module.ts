import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreditPageRoutingModule } from './credit-routing.module';

import { CreditPage } from './credit.page';
import { AccordionComponent } from 'src/app/components/accordion/accordion.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreditPageRoutingModule
  ],
  declarations: [CreditPage, AccordionComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditPageModule {}
