import { LoadingController, AlertController, ActionSheetController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  formLogin: FormGroup;
  errorLoging: boolean;
  message: string ;
  lang: string ;

  loading: any;

  constructor() { }

  ngOnInit() {
  }

}
