import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuccessRegistrationPage } from './success-registration.page';

const routes: Routes = [
  {
    path: '',
    component: SuccessRegistrationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuccessRegistrationPageRoutingModule {}
