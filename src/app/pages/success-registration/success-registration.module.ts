import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuccessRegistrationPageRoutingModule } from './success-registration-routing.module';

import { SuccessRegistrationPage } from './success-registration.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuccessRegistrationPageRoutingModule
  ],
  declarations: [SuccessRegistrationPage]
})
export class SuccessRegistrationPageModule {}
