import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-trajets',
  templateUrl: './trajets.page.html',
  styleUrls: ['./trajets.page.scss'],
})
export class TrajetsPage implements OnInit {

  data: Array<{title: string; details: string; textButton: string; showDetails: boolean}> = [];

  constructor(public navCtrl: NavController) {
    for(let i = 0; i < 2; i++ ){
      this.data.push({
          title: 'Hlm Grand Medine',
          details: ' idatat non proident, sunt in culpa qui of',
          textButton: 'SEE DETAILS',
          showDetails: false
        });
    }
  }

  ngOnInit() {
  }
  toggleDetails(data) {
    if (data.showDetails) {
        data.showDetails = false;
        data.textButton = 'SEE DETAILS';
    } else {
        data.showDetails = true;
        data.textButton = 'HIDE DETAILS';
    }
  }
}
