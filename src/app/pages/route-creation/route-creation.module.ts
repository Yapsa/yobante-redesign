import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RouteCreationPageRoutingModule } from './route-creation-routing.module';

import { RouteCreationPage } from './route-creation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouteCreationPageRoutingModule
  ],
  declarations: [RouteCreationPage]
})
export class RouteCreationPageModule {}
