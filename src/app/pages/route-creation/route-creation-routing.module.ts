import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RouteCreationPage } from './route-creation.page';

const routes: Routes = [
  {
    path: '',
    component: RouteCreationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RouteCreationPageRoutingModule {}
