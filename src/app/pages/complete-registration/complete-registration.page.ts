import { Component, OnInit } from '@angular/core';
import { countries } from 'src/app/shared/components/country-data-store';

@Component({
  selector: 'app-complete-registration',
  templateUrl: './complete-registration.page.html',
  styleUrls: ['./complete-registration.page.scss'],
})
export class CompleteRegistrationPage implements OnInit {

  countries: any = countries;
  segment: string;
  private currentNumber = 20;

  constructor() { }

  ngOnInit() {
  }
  goTransport() {
    this.segment = 'transport';
  }
  backAdresse() {
    this.segment = 'address';
  }

  goValidation() {
    this.segment = 'validation';
  }
  backTransport() {
    this.segment = 'transport';
  }

  increment() {
    this.currentNumber++;
  }

  private decrement() {
    this.currentNumber--;
  }
}
