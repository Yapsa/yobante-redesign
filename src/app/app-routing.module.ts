import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'carrier',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    loadChildren: () => import('./index/index.module').then( m => m.IndexPageModule)
  },
  {
    path: 'welcome',
    loadChildren: () => import('./pages/welcome/welcome.module').then( m => m.WelcomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./pages/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'password',
    loadChildren: () => import('./pages/password/password.module').then( m => m.PasswordPageModule)
  },
  {
    path: 'agreement',
    loadChildren: () => import('./pages/agreement/agreement.module').then( m => m.AgreementPageModule)
  },
  {
    path: 'reset-redirection',
    loadChildren: () => import('./pages/reset-redirection/reset-redirection.module').then( m => m.ResetRedirectionPageModule)
  },
  {
    path: 'complete-registration',
    loadChildren: () => import('./pages/complete-registration/complete-registration.module').then( m => m.CompleteRegistrationPageModule)
  },
  {
    path: 'success-registration',
    loadChildren: () => import('./pages/success-registration/success-registration.module').then( m => m.SuccessRegistrationPageModule)
  },
  {
    path: 'modal-yes',
    loadChildren: () => import('./pages/modal-yes/modal-yes.module').then( m => m.ModalYesPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./pages/settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'route-creation',
    loadChildren: () => import('./pages/route-creation/route-creation.module').then( m => m.RouteCreationPageModule)
  },
  {
    path: 'parcels-list',
    loadChildren: () => import('./pages/parcels-list/parcels-list.module').then( m => m.ParcelsListPageModule)
  },
  {
    path: 'parcels-details',
    loadChildren: () => import('./pages/parcels-details/parcels-details.module').then( m => m.ParcelsDetailsPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
